
Navigate dashboard as a binder

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fbitbucket.org%2Ffsmeraldi%2Fcoviddash/master?urlpath=%2Fvoila%2Frender%2FDashboard.ipynb)

Launch dashboard as a Heroku app

[https://mebo-covid-dash.herokuapp.com/](https://mebo-covid-dash.herokuapp.com/)

